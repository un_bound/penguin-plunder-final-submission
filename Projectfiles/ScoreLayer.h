//
//  ScoreLayer.h
//  TurtleBanditProto
//
//  Created by MIT on 1/30/13.
//
//

#import <Foundation/Foundation.h>

@interface ScoreLayer : CCLayer{
    int current_Level;
    int score;
}
+(id) scene:(int)num;

@end
