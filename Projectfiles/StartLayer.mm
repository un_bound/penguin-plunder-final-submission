//
//  StartLayer.m
//  ErroringOutBox2DProject
//
//  Created by OEIT on 1/23/13.
//
//

#import "StartLayer.h"
#import "GameLayer.h"
#import "SimpleAudioEngine.h"



@implementation StartLayer

const int levelCosts[9] = {0, 1, 5, 8, 11, 13, 16, 19, 22};
int totalScore;

-(id) init
{
    if ((self = [super init]))
	{
        /*CCMenu * turtleMenu = [CCMenu menuWithItems:nil];
        CCMenuItemImage *firstMenuItem = [CCMenuItemImage itemWithNormalImage:@"segment.png" selectedImage:@"blue-circle.png" target:self selector:@selector(playButtonAction)];
        [turtleMenu addChild:firstMenuItem];
        [self addChild:turtleMenu];*/
        
        //[self clearUserData];
        
        CCSprite *menu = [CCSprite spriteWithFile:@"Level page v3.png"];
        menu.position = CGPointMake(240.0f, 160.0f);
        [self addChild:menu];
        [self scheduleUpdate];
        SimpleAudioEngine *audioEngine = [SimpleAudioEngine sharedEngine];
        [audioEngine preloadBackgroundMusic:@"fumbleFunk.mp3"];
        [audioEngine playBackgroundMusic:@"fumbleFunk.mp3"];
        audioEngine.backgroundMusicVolume = .5f;
        
        totalScore = 0;
        for(int i = 0; i < 9; i++)
        {
            int levelScore = [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"highScore%d", i+1]] intValue];
            totalScore += levelScore;
            CCSprite *icon;
            int buttonX = (i%3)*150 + 50;
            int buttonY = 180- (i/3)*65;
            if(totalScore<levelCosts[i])
            {
                icon = [CCSprite spriteWithFile:@"lock.png"];
                icon.position = ccp(buttonX + 75, buttonY);
                [self addChild:icon];
            }
            
            for(int s = 0; s<levelScore; s++)
            {
                icon = [CCSprite spriteWithFile:@"starSmall.png"];
                icon.position = ccp(buttonX + s * 30, buttonY);
                [self addChild:icon];
            }
            
        }
        
    }
    return self;
}

-(void) clearUserData
{
    for(int i = 0; i < 9; i++)
    {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:0] forKey:[NSString stringWithFormat:@"highScore%d", i+1]];
    }
}

+(id) scene
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	StartLayer *layer = [StartLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
    return scene;
}

-(void)playButtonAction
{
    
    [[CCDirector sharedDirector] replaceScene:[GameLayer scene:Current_Level]];
    
    /*    LevelSelectMenu * myTempLevelSelectMenu = [LevelSelectMenu node];
     //pass myValue from this scene to the LevelSelectMenu
     [LevelSelectMenu setSomeValue:myValue];
     [[Director sharedDirector] replaceScene:myTempLevelSelectMenu];*/
}
- (void)update:(ccTime)Delta
{
    KKInput* input = [KKInput sharedInput];
    if(input.anyTouchBeganThisFrame)
    {
        // [input locationOfAnyTouchInPhase:KKTouchPhaseBegan];
    }
    else if(input.anyTouchEndedThisFrame)
    {
        CGPoint pt = [input locationOfAnyTouchInPhase:KKTouchPhaseEnded];
        NSLog(@"x:%f y:%f",pt.x,pt.y);
        if(pt.x>21.0f && pt.x<471.0f && pt.y>16.0f && pt.y<237.0f)
        {
            
            int x = (int)(pt.x-21.0f)/150.0f + 1;
            int y = 3-((int)(pt.y-16.0f)/77.0f);
            
            Current_Level = y*3 + x;

            [[CCDirector sharedDirector] replaceScene:[CCTransitionMoveInB transitionWithDuration:0.5f scene:[GameLayer scene:Current_Level]]];
        }
    }
    
}

@end