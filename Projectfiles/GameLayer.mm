/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim.
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */

#import "GameLayer.h"
#import "Box2DDebugLayer.h"
#import "SimpleAudioEngine.h"
#import "StartLayer.h"
#import "Gem.h"

//Pixel to metres ratio. Box2D uses metres as the unit for measurement.
//This ratio defines how many pixels correspond to 1 Box2D "metre"
//Box2D is optimized for objects of 1x1 metre therefore it makes sense
//to define the ratio so that your most common object type is 1x1 metre.
//hi
const float PTM_RATIO = 32.0f;
const float SPRITE_SCALE = 0.1f;
const int TILESIZE = 32;
const int TILESET_COLUMNS = 9;
const int TILESET_ROWS = 19;
const int MAX_VERTICES = 200;
const float FRICTION = 0.001f;
CGPoint prevPoint;
CGPoint prevPosTop;
CGPoint prevPosBot;
b2Vec2 VertexArray[MAX_VERTICES];
float minSegmentLen = 10.0f;
float timeStep = 0.0f;
float pi = 3.14159265;
float c_radius = 20.0f;
int dir_X[4] = {1,-1,0,0};
int dir_Y[4] = {0,0,1,-1};
BOOL playing = NO;
NSMutableSet *segments;
NSMutableSet *iceChunks;
NSMutableSet *seedPoints;
NSMutableArray *Gadgets;
NSMutableArray *Gadgets_Type;

ALuint scrapingIceID = 0;

@interface GameLayer (PrivateMethods)
-(void) enableBox2dDebugDrawing;
-(void) addSomeJoinedBodies:(CGPoint)pos;
-(void) addCircleSpriteAt:(CGPoint)p;
-(b2Vec2) toMeters:(CGPoint)point;
-(CGPoint) toPixels:(b2Vec2)vec;
@end

@implementation GameLayer

-(id) init
{
	if ((self = [super init]))
	{
        Current_State = 1;
        playing = NO;
        start_Flag = false;
		CCLOG(@"%@ init", NSStringFromClass([self class]));
		glClearColor(0.1f, 0.0f, 0.2f, 1.0f);
		
		// Construct a world object, which will hold and simulate the rigid bodies.
		b2Vec2 gravity = b2Vec2(0.0f, -10.0f);
		world = new b2World(gravity);
		world->SetAllowSleeping(YES);
		//world->SetContinuousPhysics(YES);
		[self pausePhysics];
        
		// uncomment this line to draw debug info
//        [self enableBox2dDebugDrawing];
        
		contactListener = new ContactListener();
		world->SetContactListener(contactListener);
		
		// for the screenBorder body we'll need these values
		CGSize screenSize = [CCDirector sharedDirector].winSize;
		float widthInMeters = screenSize.width / PTM_RATIO;
		float heightInMeters = screenSize.height / PTM_RATIO;
		b2Vec2 lowerLeftCorner = b2Vec2(0, 0);
		b2Vec2 lowerRightCorner = b2Vec2(widthInMeters, 0);
		b2Vec2 upperLeftCorner = b2Vec2(0, heightInMeters);
		b2Vec2 upperRightCorner = b2Vec2(widthInMeters, heightInMeters);
		
		// Define the static container body, which will provide the collisions at screen borders.
		b2BodyDef screenBorderDef;
		screenBorderDef.position.Set(0, 0);
		b2Body* screenBorderBody = world->CreateBody(&screenBorderDef);
		b2EdgeShape screenBorderShape;
		
		// Create fixtures for the four borders (the border shape is re-used)
		screenBorderShape.Set(lowerLeftCorner, lowerRightCorner);
		screenBorderBody->CreateFixture(&screenBorderShape, 0);
		screenBorderShape.Set(lowerRightCorner, upperRightCorner);
		screenBorderBody->CreateFixture(&screenBorderShape, 0);
		screenBorderShape.Set(upperRightCorner, upperLeftCorner);
		screenBorderBody->CreateFixture(&screenBorderShape, 0);
		screenBorderShape.Set(upperLeftCorner, lowerLeftCorner);
		screenBorderBody->CreateFixture(&screenBorderShape, 0);
        
        CCSprite *background = [CCSprite spriteWithFile:@"LevelBackground.png"];
        background.anchorPoint = ccp(0,0);
        background.position = ccp(0,0);
        [self addChild:background z:0];
        
        gray = [CCSprite spriteWithFile:@"grayOut.png"];
        gray.anchorPoint = ccp(0,0);
        gray.position = ccp(0,0);
        gray.visible = NO;
        [self addChild:gray z:40];
        
       /* CCMenu * controlMenu = [CCMenu menuWithItems:nil];
        CCMenuItemImage *goButton = [CCMenuItemImage itemWithNormalImage:@"segment.png" selectedImage:@"blue-circle.png" target:self selector:@selector(goButtonAction)];
        goButton.position = ccp(screenSize.width/2-50, screenSize.height/2-20);
        [controlMenu addChild:goButton];
        CCMenuItemImage *resetButton = [CCMenuItemImage itemWithNormalImage:@"segment.png" selectedImage:@"blue-circle.png" target:self selector:@selector(resetButtonAction)];
        resetButton.position = ccp(screenSize.width/2-50, screenSize.height/2-70);
        [controlMenu addChild:resetButton];
        [self addChild:controlMenu];*/
        
        
        gems = [[NSMutableArray alloc] init];
        Gadgets = [[NSMutableArray alloc] init];
        Gadgets_Type = [[NSMutableArray alloc] init];
        segments = [[NSMutableSet alloc] init];
        iceChunks = [[NSMutableSet alloc] init];
        seedPoints = [[NSMutableSet alloc] init];
        
        //[self createSpringAtPosition:ccp(100,100) andDegreeAngle:0];
        
        SimpleAudioEngine *audioEngine = [SimpleAudioEngine sharedEngine];
        [audioEngine preloadEffect:@"explo2.wav"];
        [audioEngine preloadEffect:@"scraping-ice.wav"];
        [audioEngine preloadEffect:@"penguinNoises.wav"];
        [audioEngine setBackgroundMusicVolume:.5f];
        [audioEngine playBackgroundMusic:@"penguinDance.mp3"];
        b2BodyDef floorDef;
		floorDef.position.Set(0, 0);
		[KKInput sharedInput].accelerometerActive = YES;
        [self scheduleUpdate];
	}
    
	return self;
}

-(void) goButtonAction
{
    /*[self traceIce];
    [self resumePhysics];
    playing = YES;*/
}

-(void) resetButtonAction
{
    //[self reloadLevel];
}

-(void) reloadLevel
{
    [[CCDirector sharedDirector] replaceScene:[GameLayer scene: Current_Level]];
    
}

-(void) dealloc
{
	delete contactListener;
	delete world;
    
#ifndef KK_ARC_ENABLED
	[super dealloc];
#endif
}

-(void) importLevel
{
    
    NSLog(@"CL4:%d",Current_Level);
    //Current_Level = 2;
    NSString *t_Level = [NSString stringWithFormat:@"Level%d",Current_Level];
    NSLog(@"%@",t_Level);
    NSLog(@"importlevel:%d",Current_Level);
    NSString *path = [[NSBundle mainBundle] pathForResource:t_Level ofType:@"plist"];
    NSDictionary *level = [NSDictionary dictionaryWithContentsOfFile:path];
    NSDictionary *start = [level objectForKey:@"Start"];
    penguinSprite = [CCSprite spriteWithFile:@"Penguin.png"];
    penguinStartPos = ccp([[start objectForKey:@"x"] floatValue], [[start objectForKey:@"y"] floatValue]);
    penguinSprite.position = penguinStartPos;
    penguinSprite.rotation = -60;
    
    [self newPenguinBody];
    [self addChild:penguinSprite z: 4];
    
    
    NSLog(@"172 %d",Current_Level);
    
    
    NSDictionary *goal = [level objectForKey:@"Goal"];
    goalSprite = [CCSprite spriteWithFile:@"tunnel.png"];
    goalSprite.position = ccp([[goal objectForKey:@"x"] floatValue], [[goal objectForKey:@"y"] floatValue]);
    [self addChild:goalSprite z:2];
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"sparkle.plist"];
    
    CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"sparkle.png"];
    
    [self addChild:spriteSheet];
    
    sparkleFrames = [NSMutableArray array];
    
    for(int i = 0; i <= 17; ++i)
    {
        [sparkleFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"Sparkle%d.png", i]]];
    }
    
    CCSprite *sparkleSprite = [CCSprite spriteWithSpriteFrameName:@"Sparkle1.png"];
    sparkleSprite.position = goalSprite.position;
    CCAnimation *sparkleAnimation = [CCAnimation animationWithSpriteFrames: sparkleFrames delay:0.1f];
    sparkle = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:sparkleAnimation]];
    [sparkleSprite runAction:sparkle];
    [self addChild:sparkleSprite z:4];
    
    NSArray *collectablesData = [level objectForKey:@"Collectables"];
    NSDictionary *collectable = nil;
    numGems_notRequired = 0;
    for (int i = 0; i < (int)[collectablesData count]; i++) {
        collectable = [collectablesData objectAtIndex:i];
        CGPoint position = CGPointMake([[collectable objectForKey:@"x"] floatValue], [[collectable objectForKey:@"y"] floatValue]);
        NSString *type = [collectable objectForKey:@"type"];
        BOOL required = [[collectable objectForKey:@"required"] boolValue];
        if(!required)
        {
            numGems_notRequired += 1;
        }
        if([type isEqualToString:@"gem"])
        {
            [self createGemAtPosition:position isRequired:required];
        }
    }
    
    NSArray *physObjData = [level objectForKey:@"PhysicalObjects"];
    NSDictionary *physObj = nil;
    for (int i = 0; i < (int)[physObjData count]; i++) {
        physObj = [physObjData objectAtIndex:i];
        if([[physObj objectForKey:@"type"] isEqualToString:@"spring"])
        {
            [self createSpringAtPosition:ccp([[physObj objectForKey:@"x"] floatValue],[[physObj objectForKey:@"y"] floatValue]) andDegreeAngle:[[physObj objectForKey:@"angle"] floatValue]];
        }
        if([[physObj objectForKey:@"type"] isEqualToString:@"accel"])
        {
            [self createAccelAtPosition:ccp([[physObj objectForKey:@"x"] floatValue],[[physObj objectForKey:@"y"] floatValue]) andDegreeAngle:[[physObj objectForKey:@"angle"] floatValue]];
        }
        /*CGPoint position = CGPointMake([[physObj objectForKey:@"x"] floatValue], [[physObj objectForKey:@"y"] floatValue]);
        NSString *type = [physObj objectForKey:@"type"];
        NSString *spriteImage = [physObj objectForKey:@"sprite"];
        BOOL isStatic = [[physObj objectForKey:@"static"] boolValue];
        [self createBodyOfType:type withImage:spriteImage atPosition:position isStatic:isStatic];*/
    }
    
    [self runAction:[CCMoveTo actionWithDuration:2.0f position: CGPointZero]];
    
    
    if(Current_Level == 1)
    {
        CCSprite *hintFinger = [CCSprite spriteWithFile:@"pointingFinger.png"];
        hintFinger.anchorPoint = ccp(0.0f,1.0f);
        hintFinger.tag = 1;
        [self addChild:hintFinger z:5];
        CCMoveTo *moveDown = [CCMoveTo actionWithDuration:.3 position:ccp(240,200)];
        CCMoveTo *moveUp = [CCMoveTo actionWithDuration:1.5 position:ccp(240,300)];
        CCSequence *actionsForFinger = [CCSequence actions:moveUp, moveDown, nil];
        CCAction *repeatForFinger = [CCRepeatForever actionWithAction:actionsForFinger];
        [hintFinger runAction:repeatForFinger];
        
        CCSprite *hintFinger2 = [CCSprite spriteWithFile:@"pointingFinger.png"];
        hintFinger2.anchorPoint = ccp(0.0f,1.0f);
        hintFinger2.tag = 5;
        hintFinger2.scaleX = -1;
        hintFinger2.visible = NO;
        [self addChild:hintFinger2 z:5];
        CCMoveTo *moveDown2 = [CCMoveTo actionWithDuration:.3 position:ccp(475,300)];
        CCMoveTo *moveUp2 = [CCMoveTo actionWithDuration:1.5 position:ccp(460,300)];
        CCSequence *actionsForFinger2 = [CCSequence actions:moveUp2, moveDown2, nil];
        CCAction *repeatForFinger2 = [CCRepeatForever actionWithAction:actionsForFinger2];
        [hintFinger2 runAction:repeatForFinger2];
    }
    
    UIImage *tImage = [UIImage imageNamed:@"tIce2.png"];
    CCMutableTexture2D *tMutableTexture = [[CCMutableTexture2D alloc] initWithImage:tImage];
    [tMutableTexture setAliasTexParameters];
    Ice_Sprite = [CCSprite spriteWithTexture:tMutableTexture];
    Ice_Sprite.position = CGPointMake(240.0f, 160.0f);
    [self addChild:Ice_Sprite];
    CCMutableTexture2D *tTexture = (CCMutableTexture2D*)(Ice_Sprite.texture);
    
    for(int i=0;i<480;i++)
    {
        for(int j=0;j<320;j++)
        {
            ice_pixels[i][j] = 1;
        }
    }
    
    NSLog(@"242 %d",Current_Level);
    //////////////////////////////////////////
    ///              Ice Remove            ///
    //////////////////////////////////////////
    
    NSArray *Ice_Remove_Data = [level objectForKey:@"Ice_Remove"];
    NSDictionary *t_Ice_Remove = nil;
    int tlevel = Current_Level;
    
    NSLog(@"249 %d",Current_Level);
    for (int i = 0; i < (int)[Ice_Remove_Data count]; i++) {
        t_Ice_Remove = [Ice_Remove_Data objectAtIndex:i];
        NSString *type = [t_Ice_Remove objectForKey:@"type"];
        if([type isEqualToString:@"rect"])
        {
            int x = [[t_Ice_Remove objectForKey:@"x"] intValue];
            int y = [[t_Ice_Remove objectForKey:@"y"] intValue];
            int h_length = [[t_Ice_Remove objectForKey:@"length"] intValue]/2;
            int h_width = [[t_Ice_Remove objectForKey:@"height"] intValue]/2;
            for(int i=-h_length;i<h_length+1;i++)
            {
                for(int j=-h_width;j<h_width+1;j++)
                {
                    ice_pixels[x+i][y+j] = 0;
                    [tTexture setPixelAt:CGPointMake(x+i, 320-(y+j)) rgba:ccc4(0,0,0,0)];
                    
                }
            }
        }
        else if([type isEqualToString:@"circ"])
        {
            int x = [[t_Ice_Remove objectForKey:@"x"] intValue];
            int y = [[t_Ice_Remove objectForKey:@"y"] intValue];
            int radius = [[t_Ice_Remove objectForKey:@"radius"] intValue];
            NSLog(@"rad:%d",radius);
            float f_radius = (float)radius;
            for(int i = -radius;i<radius+1;i++)
            {
                int r_y = (int)sqrtf(f_radius*f_radius - ((float)i)*((float)i));
                for(int j=-r_y;j<r_y+1;j++)
                {
                    ice_pixels[x+i][y+j] = 0;
                    [tTexture setPixelAt:CGPointMake(x+i, 320-(y+j)) rgba:ccc4(0,0,0,0)];
                }
            }
        }
        else if([type isEqualToString:@"tri"])
        {
            
        }
    }
    
    Current_Level = tlevel;
    
    
    NSLog(@"294 %d",Current_Level);
    
    
    
    ////////////////////\\\\\\\\\\\\\\\\\\\\
    //|              Ice Fill            |||
    //\\\\\\\\\\\\\\\\\\////////////////////
    NSArray *Ice_Fill_Data = [level objectForKey:@"Ice_Fill"];
    NSDictionary *t_Ice_Fill = nil;
    for (int i = 0; i < (int)[Ice_Fill_Data count]; i++) {
        t_Ice_Fill = [Ice_Fill_Data objectAtIndex:i];
        NSString *type = [t_Ice_Fill objectForKey:@"type"];
        if([type isEqualToString:@"rect"])
        {
            int x = [[t_Ice_Fill objectForKey:@"x"] intValue];
            int y = [[t_Ice_Fill objectForKey:@"y"] intValue];
            int h_length = [[t_Ice_Fill objectForKey:@"length"] intValue]/2;
            int h_width = [[t_Ice_Fill objectForKey:@"height"] intValue]/2;
            for(int i=-h_length;i<h_length+1;i++)
            {
                for(int j=-h_width;j<h_width+1;j++)
                {
                    ice_pixels[x+i][y+j] = 1;
                    [tTexture setPixelAt:CGPointMake(x+i, 320-(y+j)) rgba:ccc4(0,255,255,255)];
                    
                }
            }
        }
        else if([type isEqualToString:@"circ"])
        {
            int x = [[t_Ice_Fill objectForKey:@"x"] intValue];
            int y = [[t_Ice_Fill objectForKey:@"y"] intValue];
            int radius = [[t_Ice_Fill objectForKey:@"radius"] intValue];
            NSLog(@"rad:%d",radius);
            float f_radius = (float)radius;
            for(int i = -radius;i<radius+1;i++)
            {
                int r_y = (int)sqrtf(f_radius*f_radius - ((float)i)*((float)i));
                for(int j=-r_y;j<r_y+1;j++)
                {
                    ice_pixels[x+i][y+j] = 1;
                    [tTexture setPixelAt:CGPointMake(x+i, 320-(y+j)) rgba:ccc4(0,255,255,255)];
                }
            }
        }
        else if([type isEqualToString:@"tri"])
        {
            
        }
        
    }
    
    [tTexture apply];
    NSLog(@"347 %d",Current_Level);
    
    menu_Button = [CCSprite spriteWithFile:@"tempbutton.png"];
    menu_Button.position = CGPointMake(460.0f, 300.0f);
    [self addChild:menu_Button];
    
}

-(void) resetPenguin
{
    playing = NO;
    [self pausePhysics];
    penguinSprite.position = penguinStartPos;
    penguinSprite.rotation = -60;
    world->DestroyBody(penguinBody);
    [self newPenguinBody];
}

-(void) newPenguinBody
{
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position = [self toMeters:penguinStartPos];
    bodyDef.userData = (__bridge void*)penguinSprite;
    bodyDef.angle = CC_DEGREES_TO_RADIANS(60);
    penguinBody = world->CreateBody(&bodyDef);
    
    b2PolygonShape penguinShape;
    b2Vec2 penguinVertices[8];
    int coordinates[8][2] = {{7, 19}, {8, 10}, {16, 3}, {32, 2}, {40, 6}, {44, 14}, {42, 19}, {11, 22}};
    
    for(int i = 0; i<8; i++)
    {
        float x = (coordinates[i][0]-25)/PTM_RATIO;
        float y = (13-coordinates[i][1])/PTM_RATIO;
        penguinVertices[7-i].Set(x, y);
    }
    
    penguinShape.Set(penguinVertices, 8);
    
    //    b2CircleShape penguinShape;
    //    penguinShape.m_radius = penguinSprite.contentSize.width/2.0f/PTM_RATIO/1.5f;
    // Define the dynamic body fixture.
    b2FixtureDef penguinFixture;
    penguinFixture.shape = &penguinShape;
    penguinFixture.density = 0.3f;
    penguinFixture.friction = 0.0f;
    penguinFixture.restitution = 0.0f;
    NSLog(@"198 %d",Current_Level);
    penguinBody->CreateFixture(&penguinFixture);
}

-(void) createGemAtPosition:(CGPoint) pos isRequired:(BOOL) req
{
    Gem *newGem;
    if(req)
    {
        newGem = [[Gem alloc] initWithRequiredSprite];
    }
    else
    {
        newGem = [[Gem alloc] initWithRandomSprite];
    }
    newGem.position = pos;
    newGem.required = req;
    [gems addObject:newGem];
    [self addChild:newGem];
    
}

-(void) createSpringAtPosition:(CGPoint) pos andDegreeAngle:(float) angle
{
    CCSprite *spring;
    spring = [CCSprite spriteWithFile:@"spring.png"];
    spring.position = pos;
    spring.rotation = angle;
    [Gadgets addObject:spring];
    [Gadgets_Type addObject:@"spring"];
    [self addChild:spring];
    
}
- (void) createAccelAtPosition:(CGPoint) pos andDegreeAngle:(float) angle
{
    CCSprite *accel;
    accel = [CCSprite spriteWithFile:@"tempaccel.png"];
    accel.position = pos;
    accel.rotation = angle;
    [Gadgets addObject:accel];
    [Gadgets_Type addObject:@"accel"];
    [self addChild:accel];
}
-(void) createBodyOfType: (NSString*) type withImage: (NSString*) imageName atPosition: (CGPoint) pos isStatic: (BOOL) isStatic
{
    b2Body *body;
    CCSprite *sprite = [CCSprite spriteWithFile:imageName];
	sprite.position = pos;
	b2BodyDef bodyDef;
    if(isStatic)
    {
        bodyDef.type = b2_staticBody;
        
    }
    else
    {
        bodyDef.type = b2_dynamicBody;
        
    }
    bodyDef.position = [self toMeters:pos];
    bodyDef.userData = (__bridge void*)sprite;
    body = world->CreateBody(&bodyDef);
    
    b2FixtureDef fixtureDef;
    
    if([type isEqualToString: @"circle"]){
        b2CircleShape circle;
        float radius = sprite.contentSize.width/2.0f*SPRITE_SCALE/PTM_RATIO;
        circle.m_radius = radius;
        fixtureDef.shape = &circle;
    }
    else if([type isEqualToString: @"rectangle"]){
        b2PolygonShape rect;
        rect.SetAsBox(sprite.contentSize.width/2.0f/PTM_RATIO, sprite.contentSize.height/2.0f/PTM_RATIO);
        fixtureDef.shape = &rect;
    }
    
	
	// Define the dynamic body fixture.
	fixtureDef.density = 0.3f;
	fixtureDef.friction = 0.0f;
	fixtureDef.restitution = 0.0f;
    
	body->CreateFixture(&fixtureDef);
    [self addChild:sprite];
    
}


-(BOOL) floodFillatX:(int) x andY:(int) y
{
    
    if((x<0)||(y<0)||(x>=480)||(y>=320)||(ice_pixels[x][y] != 1)) return NO;
    
    //return NO;
    //NSLog(@"x:%d y:%d",x,y);
    //return NO;
    if(ice_pixels[x][y] != 1)
    {
        return NO;
    }
    NSMutableArray *lastPoint_Save_X = [[NSMutableArray alloc] init];
    NSMutableArray *lastPoint_Save_Y = [[NSMutableArray alloc] init];
    int c_x = x;
    int c_y = y;
    int i = 0;
    do
    {
        // NSLog(@"cx:%d cy:%d",c_x,c_y);
        i = dir_save[c_x][c_y];
        ice_pixels[c_x][c_y] = -1;
        do
        {
            if((c_x+dir_X[i]>-1) && (c_y+dir_Y[i]>-1) && (c_x+dir_X[i]<480) && (c_y+dir_Y[i]<320))
            {
                if(ice_pixels[c_x+dir_X[i]][c_y+dir_Y[i]] == 1)
                {
                    break;
                }
            }
            i++;
        }while(i<4);
        if(i<4)
        {
            dir_save[c_x][c_y] = i+1;
            [lastPoint_Save_X addObject:[NSNumber numberWithInt:c_x]];
            [lastPoint_Save_Y addObject:[NSNumber numberWithInt:c_y]];
            c_x += dir_X[i];
            c_y += dir_Y[i];
        }
        else
        {
            if((int)[lastPoint_Save_X count]==0)
            {
                break;
            }
            c_x = [[lastPoint_Save_X objectAtIndex:((int)[lastPoint_Save_X count])-1] intValue];
            c_y = [[lastPoint_Save_Y objectAtIndex:((int)[lastPoint_Save_Y count])-1] intValue];
            [lastPoint_Save_X removeLastObject];
            [lastPoint_Save_Y removeLastObject];
        }
    }while((int)[lastPoint_Save_X count]>0);
    
    return YES;
}

-(void) traceIce
{
    gray.visible = true;
    if(scrapingIceID!=(ALuint)0)
    {
        [[SimpleAudioEngine sharedEngine] stopEffect:scrapingIceID];
        scrapingIceID = (ALuint)0;
    }
    
    CCAction *callFunction = [CCCallFunc actionWithTarget:self selector:@selector(traceIceAction)];
    CCFiniteTimeAction *wait = [CCMoveBy actionWithDuration:.1f position:CGPointZero];
    [self runAction:[CCSequence actions:wait, callFunction, nil]];

}

-(void) traceIceAction
{
    [seedPoints removeAllObjects];
    
    CCMutableTexture2D* texture = (CCMutableTexture2D*)(Ice_Sprite.texture);
    
    for(int i=0;i<480;i++)
    {
        for(int j=0;j<320;j++)
        {
            ice_pixels[i][j] = ([texture pixelAt:ccp(i,j)].a == 0)?0:1;
            dir_save[i][j] = 0;
        }
    }
    
    
    for(int i=0;i<480;i++)
    {
        for(int j=0;j<320;j++)
        {
            if([self floodFillatX:i andY:j])
            {
                [seedPoints addObject: [NSValue valueWithCGPoint:ccp(i, j)]];
            }
        }
    }
    
    
    for(int i=0;i<480;i++)
    {
        for(int j=0;j<320;j++)
        {
            if(ice_pixels[i][j] == -1) ice_pixels[i][j] = 1;
        }
    }
    
    NSEnumerator *iceEnum = [iceChunks objectEnumerator];
    NSValue *bodyPointer = [iceEnum nextObject];
    while(bodyPointer != nil)
    {
        b2Body *body = (b2Body*)[bodyPointer pointerValue];
        world->DestroyBody(body);
        bodyPointer = [iceEnum nextObject];
    }
    [iceChunks removeAllObjects];
    
    
    NSEnumerator *seedPointEnum = [seedPoints objectEnumerator];
    NSValue *seedPointPointer = [seedPointEnum nextObject];
    while(seedPointPointer != nil)
    {
        CGPoint seedPoint = [seedPointPointer CGPointValue];
        int vertexNum = [self traceRegionStartingAtX:seedPoint.x andY:seedPoint.y withStepSize:20];
        if(vertexNum >= 3)
        {
            b2BodyDef iceChunkDef;
            iceChunkDef.position.Set(0, 0);
            b2Body *iceChunkBody = world->CreateBody(&iceChunkDef);
            b2ChainShape chain;
            chain.CreateLoop(VertexArray, vertexNum);
            iceChunkBody->CreateFixture(&chain, vertexNum);
            NSValue *pointerObject = [NSValue valueWithPointer:iceChunkBody];
            [iceChunks addObject: pointerObject];
        }
        seedPointPointer = [seedPointEnum nextObject];
        
    }
    //    int vertexNum = [self traceRegionStartingAtX:479 andY:200 withStepSize:20];
    //    if(vertexNum >= 3)
    //    {
    //        b2BodyDef iceChunkDef;
    //        iceChunkDef.position.Set(0, 0);
    //        b2Body *iceChunkBody = world->CreateBody(&iceChunkDef);
    //        b2ChainShape chain;
    //        chain.CreateLoop(VertexArray, vertexNum);
    //        iceChunkBody->CreateFixture(&chain, vertexNum);
    //        NSValue *pointerObject = [NSValue valueWithPointer:iceChunkBody];
    //        [iceChunks addObject: pointerObject];
    //    }
    int debug = Current_Level;
    NSLog(@"debuglevel:%d",Current_Level);
    gray.visible = false;

    
}

-(int) traceRegionStartingAtX:(int) testPointX andY:(int) testPointY withStepSize: (int) step
{
    CCMutableTexture2D* texture = (CCMutableTexture2D*)(Ice_Sprite.texture);
    //    [texture setPixelAt:ccp(testPointX,testPointY) rgba:ccc4(255, 0, 0, 255)];
    
    if([texture pixelAt:ccp(testPointX, testPointY)].a == 0) return 0;
    
    int currentX;
    int currentY = testPointY;
    for(currentX = testPointX; currentX < 480; currentX++)
    {
        if([texture pixelAt:ccp(currentX, currentY)].a == 0) break;
        //        [texture setPixelAt:ccp(currentX, currentY) rgba:ccc4(255, 0, 0, 255)];
    }
    
    //back up one pixel and keep that value
    currentX--;
    // If not broken, currentX = 479
    
    int stopPointX = currentX;
    int stopPointY = currentY;
    int newX;
    int newY;
    int prevDir = 2;
    int dirToX[8] = {1, 1, 0, -1, -1, -1, 0, 1};
    int dirToY[8] = {0, 1, 1, 1, 0, -1, -1, -1};
    int checkDir;
    int startDir;
    int count = 0;
    int i;
    int coordinateIndex = 0;
    while (YES)
    {
        startDir = (prevDir % 2 == 0)? prevDir + 7: prevDir + 6;
        for (i = 0; i < 8; i++)
        {
            checkDir = (startDir + i) % 8;
            newX = currentX + dirToX[checkDir];
            newY = currentY + dirToY[checkDir];
            if(!(newX>-1 && newX<480 && newY>-1 && newY<320))
            {
                continue;
            }
            ccColor4B color = [texture pixelAt:ccp(newX, newY)];
            if (color.a != 0)
            {
                currentX = newX;
                currentY = newY;
                prevDir = checkDir;
                [texture setPixelAt:ccp(currentX, currentY) rgba:ccc4(100, 100, 200, 100)];
                break;
            }
        }
        if(i == 8)
        {
            return 0;
        }
        if(count % step == 0 && coordinateIndex != MAX_VERTICES)
        {
            VertexArray[coordinateIndex].Set(currentX/PTM_RATIO, (320 - currentY)/PTM_RATIO);
            coordinateIndex++;
        }
        count++;
        if(abs(currentX - stopPointX) < 3 && abs(currentY - stopPointY) < 3 && count > step && coordinateIndex != MAX_VERTICES)
        {
            VertexArray[coordinateIndex].Set(currentX/PTM_RATIO, (320 - currentY)/PTM_RATIO);
            coordinateIndex++;
            break;
        }
    }
    //
    //    //fill the remaining points with the same point over and over, to end the shape there.
    //    for (int j = coordinateIndex; j < MAX_VERTICES; j++)
    //    {
    //        VertexArray[j].Set(currentX/PTM_RATIO, (currentY-320)/PTM_RATIO);
    //    }
    
    [texture apply];
    return coordinateIndex;
}


-(void) enableBox2dDebugDrawing
{
	// Using John Wordsworth's Box2DDebugLayer class now
	// The advantage is that it draws the debug information over the normal cocos2d graphics,
	// so you'll still see the textures of each object.
	const BOOL useBox2DDebugLayer = YES;
    
	
	float debugDrawScaleFactor = 1.0f;
#if KK_PLATFORM_IOS
	debugDrawScaleFactor = [[CCDirector sharedDirector] contentScaleFactor];
#endif
	debugDrawScaleFactor *= PTM_RATIO;
    
	UInt32 debugDrawFlags = 0;
	debugDrawFlags += b2Draw::e_shapeBit;
	debugDrawFlags += b2Draw::e_jointBit;
	//debugDrawFlags += b2Draw::e_aabbBit;
	//debugDrawFlags += b2Draw::e_pairBit;
	//debugDrawFlags += b2Draw::e_centerOfMassBit;
    
	if (useBox2DDebugLayer)
	{
		Box2DDebugLayer* debugLayer = [Box2DDebugLayer debugLayerWithWorld:world
																  ptmRatio:PTM_RATIO
																	 flags:debugDrawFlags];
		[self addChild:debugLayer z:100];
	}
	else
	{
		debugDraw = new GLESDebugDraw(debugDrawScaleFactor);
		if (debugDraw)
		{
			debugDraw->SetFlags(debugDrawFlags);
			world->SetDebugDraw(debugDraw);
		}
	}
}


-(void) update:(ccTime)delta
{
    if(start_Flag)
    {
        
        [self traceIce];
        [self resumePhysics];
        playing = YES;
        start_Flag = false;
    }
    KKInput* input = [KKInput sharedInput];
    if(Current_State==1)
    {
        if(input.anyTouchBeganThisFrame)
        {
            prevPoint = [input locationOfAnyTouchInPhase:KKTouchPhaseBegan];
            prevPosTop = ccp(prevPoint.x, prevPoint.y);
            prevPosBot = ccp(prevPoint.x, prevPoint.y);
            [self pausePhysics];
        }
        else if (input.anyTouchEndedThisFrame)
        {
            
            CGPoint newPoint = [input locationOfAnyTouchInPhase:KKTouchPhaseAny];
            if(newPoint.x > 440.0f && newPoint.y > 280.0f)
            {
                menu = [CCSprite spriteWithFile:@"temppause.png"];
                menu.position = CGPointMake(405.0f, 160.0f);
                [self addChild:menu z: 45];
                Current_State = 3;
                return;
            }
            //        float deltaX = newPoint.x - prevPoint.x;
            //        float deltaY = newPoint.y - prevPoint.y;
            //        float angle = atan2f(deltaY, deltaX);
            //        //[self addCapAtPoint:newPoint atAngle:angle];
            //        [self removeInvalidSegments];
            if(playing)
            {
                [self traceIce];
                [self resumePhysics];
            }
            
            /*if(newPoint.x > 440.0f && newPoint.y < 20.0f)
            {
                [[CCDirector sharedDirector] replaceScene:[CCTransitionMoveInT transitionWithDuration:0.5f scene:[StartLayer scene]]];
            }*/
            
            
            if([self getChildByTag:1] != nil && Current_Level == 1)
            {
                [self removeChild:[self getChildByTag:1] cleanup:YES];
                [self getChildByTag:5].visible = YES;
            }
            if(scrapingIceID!=(ALuint)0)
            {
                [[SimpleAudioEngine sharedEngine] stopEffect:scrapingIceID];
                scrapingIceID = (ALuint)0;
            }
            
        }
        else if (input.anyTouchLocation.x != 0.0f && input.anyTouchLocation.y != 0.0f)
        {
            if(prevPoint.x != 0 && prevPoint.y != 0){
                CGPoint newPoint = [input locationOfAnyTouchInPhase:KKTouchPhaseAny];
                CCMutableTexture2D* tTexture=(CCMutableTexture2D*)(Ice_Sprite.texture);
                [tTexture drawLineFrom:ccp(prevPoint.x, Ice_Sprite.contentSize.height/2.0f + Ice_Sprite.position.y - prevPoint.y) to:ccp(newPoint.x, Ice_Sprite.contentSize.height/2.0f + Ice_Sprite.position.y - newPoint.y) withLineWidth:c_radius andColor:ccc4(0,0,0,0)];
                [tTexture apply];
                prevPoint = newPoint;
                if(scrapingIceID == 0)
                {
                    scrapingIceID = [[SimpleAudioEngine sharedEngine] playEffect:@"scraping-ice.wav"];
                }
            }
        }
        

        // The number of iterations influence the accuracy of the physics simulation. With higher values the
        // body's velocity and position are more accurately tracked but at the cost of speed.
        // Usually for games only 1 position iteration is necessary to achieve good results.
        int32 velocityIterations = 8;
        int32 positionIterations = 1;
        world->Step(timeStep, velocityIterations, positionIterations);
        if(playing)
            for (b2Body* body = world->GetBodyList(); body != nil; body = body->GetNext())
            {
                //get the sprite associated with the body
                CCSprite* sprite = (__bridge CCSprite*)body->GetUserData();
                if (sprite != NULL)
                {
                    // update the sprite's position to where their physics bodies are
                    sprite.position = [self toPixels:body->GetPosition()];
                    float angle = body->GetAngle();
                    sprite.rotation = CC_RADIANS_TO_DEGREES(angle) * -1;
                }
            }
        [self detectCollisions];
        if(playing)
            if(penguinSprite.position.x < goalSprite.position.x+ goalSprite.contentSize.width/2.0f && penguinSprite.position.x> goalSprite.position.x - goalSprite.contentSize.width/2.0f)
            {
                if(penguinSprite.position.y < (goalSprite.position.y + goalSprite.contentSize.height/2.0f) && penguinSprite.position.y > goalSprite.position.y - goalSprite.contentSize.height/2.0f)
                {
                    if([self collectedAllRequired])
                    {
                        //[[CCDirector sharedDirector] replaceScene:[StartLayer scene]];
                        if(numGems_notRequired==0)
                        {
                            Score = 3;
                        }
                        else
                        {
                            float gems_Collected = (float)(numGems_notRequired - ((int)[gems count]));
                            float totalGems = (float)numGems_notRequired;
                            float tScore = gems_Collected/totalGems;
                            if(tScore<=0.5f)
                            {
                                Score = 1;
                            }
                            else if(tScore<=0.75f)
                            {
                                Score = 2;
                            }
                            else if(tScore<=1.0f)
                            {
                                Score = 3;
                            }
                        }
                        playing = NO;
                        CGPoint targetPoint = ccp(goalSprite.position.x, goalSprite.position.y);
                        CCFiniteTimeAction *movePenguin = [CCMoveTo actionWithDuration:0.5f position:targetPoint];
                        CCFiniteTimeAction *scalePenguin = [CCScaleTo actionWithDuration: 0.5f scale: 0.01f];
                        CCAction *callFunction = [CCCallFunc actionWithTarget:self selector:@selector(Score_Page)];
                        [penguinSprite runAction:[CCSequence actions:movePenguin, scalePenguin, callFunction, nil]];
                        [self pausePhysics];
                    }
                    else
                    {
                        if(![self getChildByTag:2]){
                            CCSprite *collectAllReminder = [CCSprite spriteWithFile:@"CollectAllStars.png"];
                            collectAllReminder.position = ccp(240, -400);
                            collectAllReminder.tag = 2;
                            [self addChild:collectAllReminder z: 50];
                            
                            CGPoint targetPoint = ccp(240, 15);
                            CCFiniteTimeAction *moveIn = [CCMoveTo actionWithDuration:0.5f position:targetPoint];
                            [collectAllReminder runAction:moveIn];
                        }
                    }
                }
                else if([self getChildByTag:2])
                {
                    [self removeChildByTag:2 cleanup:YES];
                }
            }
    }
    else if(Current_State==2)
    {
        if (input.anyTouchEndedThisFrame)
        {
            CGPoint pt = [input locationOfAnyTouchInPhase:KKTouchPhaseAny];
            NSLog(@"x:%f y:%f",pt.x,pt.y);
            if(pt.x>67.0f && pt.x<236.0f && pt.y<118.0f && pt.y>44.0f)
            {
                [[CCDirector sharedDirector] replaceScene:[CCTransitionMoveInT transitionWithDuration:0.5f scene:[StartLayer scene]]];
                //[[CCDirector sharedDirector] replaceScene:[LevelCompleteLayer scene:Current_Level]];
            }
            if(pt.x>236.0f && pt.x<410.0f && pt.y<118.0f && pt.y>44.0f)
            {
                //next level
                [[CCDirector sharedDirector] replaceScene:[CCTransitionFadeBL transitionWithDuration:1.0f scene:[GameLayer scene:Current_Level+1]]];
            }
        }
    }
    else if(Current_State==3)
    {
        if (input.anyTouchEndedThisFrame)
        {
            CGPoint pt = [input locationOfAnyTouchInPhase:KKTouchPhaseAny];
            if(pt.x<330.0f)
            {
                [self removeChild:menu cleanup:YES];
                Current_State = 1;
            }
            else
            {
                if([self getChildByTag:5] && Current_Level == 1)
                [self removeChild:[self getChildByTag:5] cleanup:YES];

                if(pt.y>240.0f)
                {
                    start_Flag = true;
                    penguinBody->ApplyAngularImpulse(-.04); //flip the penguin gently head-first!
                    //self add image loading
                }
                else if(pt.y>160.0f)
                {
                    [self resetPenguin];
                }
                else if(pt.y>80.0f)
                {
                    [self reloadLevel];
                }
                else
                {
                    [[CCDirector sharedDirector] replaceScene:[CCTransitionMoveInT transitionWithDuration:0.5f scene:[StartLayer scene]]];
                }
                [self removeChild:menu cleanup:YES];
                Current_State = 1;
            }
        }
        if(scrapingIceID!=(ALuint)0)
        {
            [[SimpleAudioEngine sharedEngine] stopEffect:scrapingIceID];
            scrapingIceID = (ALuint)0;
        }
    }
}

-(void) detectCollisions
{
    for(int j = 0; j < (int)[gems count]; j++)
    {
        NSInteger gemIndex = j;
        Gem *gem = [gems objectAtIndex:gemIndex];
        
        //            firstrect = [gemSprite textureRect];
        //            secondrect = [penguinSprite textureRect];
        //check if their x coordinates match
        if(penguinSprite.position.x < gem.position.x+ gem.contentSize.width/2.0f && penguinSprite.position.x> gem.position.x - gem.contentSize.width/2.0f)
        {
            //check if their y coordinates are within the height of the block
            if(penguinSprite.position.y < (gem.position.y + gem.contentSize.height/2.0f) && penguinSprite.position.y > gem.position.y - gem.contentSize.height/2.0f)
            {
                [self removeChild:gem cleanup:YES];
                [gems removeObjectAtIndex:gemIndex];
                [[SimpleAudioEngine sharedEngine] playEffect:@"explo2.wav"];
                
            }
        }
        
    }
    for(int j = 0; j < (int)[Gadgets count]; j++)
    {
        NSInteger gadgetIndex = j;
        CCSprite *tGadget = [Gadgets objectAtIndex:gadgetIndex];
        if(penguinSprite.position.x < tGadget.position.x+ tGadget.contentSize.width/2.0f && penguinSprite.position.x> tGadget.position.x - tGadget.contentSize.width/2.0f)
        {
            if(penguinSprite.position.y < (tGadget.position.y + tGadget.contentSize.height/2.0f) && penguinSprite.position.y > tGadget.position.y - tGadget.contentSize.height/2.0f)
            {
                if([[Gadgets_Type objectAtIndex:j] isEqualToString:@"spring"])
                {
                    b2Vec2 initialVelocity = penguinBody->GetLinearVelocity();
                    float initialX = initialVelocity.x;
                    float initialY = initialVelocity.y;
                    float angle = -1*CC_DEGREES_TO_RADIANS(tGadget.rotation);
                    float nHatX = -sinf(angle);
                    float nHatY = cosf(angle);
                    float dotProduct = initialX*nHatX + initialY*nHatY;
                    float stiffness = 25;
                    float magnitude = -1*dotProduct*stiffness;
                    //                xprime - x0 = x0 - x1 + 2nhat((x1-x0) dot nhat)
                    //penguinBody->ApplyForceToCenter(b2Vec2(-10*finalX,-10*finalY));
                    if(dotProduct < 0)
                    penguinBody->ApplyForceToCenter(b2Vec2(nHatX*magnitude,nHatY*magnitude));
                    
                    //                [[SimpleAudioEngine sharedEngine] playEffect:@"somekindofGadgetsound.wav"];
                }
                else if([[Gadgets_Type objectAtIndex:j] isEqualToString:@"accel"])
                {
                    b2Vec2 initialVelocity = penguinBody->GetLinearVelocity();

                    float angle = CC_DEGREES_TO_RADIANS(tGadget.rotation);
                    float nHatX = sinf(angle);
                    float nHatY = cosf(angle);
                    penguinBody->ApplyForceToCenter(b2Vec2(nHatX*20,nHatY*20));

                }
                
            }
        }
        
    }
    
    
}

-(BOOL) collectedAllRequired
{
    if([gems count]==0) return YES;
    for(int j = 0; j < (int)[gems count]; j++)
    {
        NSInteger gemIndex = j;
        Gem *gem = [gems objectAtIndex:gemIndex];
        if(gem.required) return NO;
    }
    return YES;
}

-(void) pausePhysics
{
    timeStep = 0;
}

-(void) resumePhysics
{
    timeStep = .03;
}


- (void)Score_Page
{
    int bestScore = [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"highScore%d", Score]] intValue];
    if(bestScore < Score)
    {
        NSNumber *newScore = [NSNumber numberWithInteger:Score];
        [[NSUserDefaults standardUserDefaults] setObject:newScore forKey:[NSString stringWithFormat:@"highScore%d", Score]];

    }
    CCSprite *score_Background = [CCSprite spriteWithFile:@"levelcomplete2.png"];
    score_Background.position = CGPointMake(240.0f, 160.0);
    [self addChild:score_Background z:50];
    
    for(int i=0;i<Score;i++)
    {
        CCSprite *tStar = [CCSprite spriteWithFile:@"star2.png"];
        tStar.position = CGPointMake(240.0f + i*80.0f - 80.0f, 180.0f);
        [self addChild:tStar z:51];
    }
    CCSprite *happyPenguin = [CCSprite spriteWithFile:@"happyPenguin.png"];
    happyPenguin.position = ccp(400, -400);
    [self addChild:happyPenguin z: 50];
    
    CGPoint targetPoint = ccp(400, 0);
    CCFiniteTimeAction *movePenguin = [CCMoveTo actionWithDuration:0.5f position:targetPoint];
    [happyPenguin runAction:movePenguin];
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"LevelComplete.mp3"];
    [[SimpleAudioEngine sharedEngine] playEffect:@"penguinNoises.wav"];
    
    Current_State = 2;
}


// convenience method to convert a CGPoint to a b2Vec2
-(b2Vec2) toMeters:(CGPoint)point
{
	return b2Vec2(point.x / PTM_RATIO, point.y / PTM_RATIO);
}

// convenience method to convert a b2Vec2 to a CGPoint
-(CGPoint) toPixels:(b2Vec2)vec
{
	return ccpMult(CGPointMake(vec.x, vec.y), PTM_RATIO);
}

+(id) scene: (int) level
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GameLayer *layer = [GameLayer node];
	[layer setLevel:level];
	// add layer as a child to scene
	[scene addChild: layer];
	
    
	// return the scene
    return scene;
}

-(void) setLevel: (int) level {
    NSLog(@"what: %d",level);
    Current_Level = level;
    [self importLevel];
    //[self traceIce];
}


//#if DEBUG
//-(void) draw
//{
//	[super draw];
//
//	if (debugDraw)
//	{
//		ccGLEnableVertexAttribs(kCCVertexAttribFlag_Position);
//		kmGLPushMatrix();
//		world->DrawDebugData();
//		kmGLPopMatrix();
//	}
//}
//#endif

//-(void) addSegmentAtPoint:(CGPoint)pos withWidth:(float) width atRadianAngle:(float) angle
//{
//    b2Body *body;
//
//    CCSprite *sprite = [CCSprite spriteWithFile:@"segment.png"];
//    sprite.anchorPoint = ccp(0.5, 0.5);
//    sprite.position = pos;
//    sprite.opacity = 0;
//    b2BodyDef bodyDef;
//    bodyDef.type = b2_staticBody;
//    bodyDef.position = [self toMeters:pos];
//    bodyDef.angle = angle;
//    bodyDef.userData = (__bridge void*)sprite;
//    body = world->CreateBody(&bodyDef);
//    b2PolygonShape box;
//    box.SetAsBox(width/PTM_RATIO, 2.0f/PTM_RATIO);//sprite.contentSize.height/PTM_RATIO/2.0f);
//    b2FixtureDef fixtureDef;
//    fixtureDef.shape = &box;
//    fixtureDef.density = 0.3f;
//    fixtureDef.friction = FRICTION;
//
//    body->CreateFixture(&fixtureDef);
//    [self addChild:sprite];
//    [segments addObject:[NSValue valueWithPointer:body]];
//}
//
//-(void) addCapAtPoint:(CGPoint) pos atAngle:(float) angle
//{
//    float angleStep = atan2f(minSegmentLen, c_radius);
//    for (float a = 0; a<=pi; a += angleStep) {
//        [self addSegmentAtPoint:ccp(pos.x + c_radius * cosf(a + angle), pos.y + c_radius * sinf(a + angle)) withWidth:minSegmentLen atRadianAngle:a + angle + pi/2.0f];
//    }
//}
//
//-(BOOL) checkInvalidSegment:(b2Body*) segment
//{
//    float radius = 10;
//    CCMutableTexture2D* texture = (CCMutableTexture2D*)(Ice_Sprite.texture);
//
//    for(float angle = 0.0f; angle < 2.0f*pi; angle += pi/8.0f)
//    {
//        CGPoint segmentPos = [self toPixels:segment->GetPosition()];
//        int x = segmentPos.x + radius*cosf(angle);
//        int y = segmentPos.y + radius*sinf(angle);
//        ccColor4B color = [texture pixelAt: ccp(x, 320 - y)];
//        if(color.a != 0) return NO;
//    }
//    return YES;
//}
//
//-(void) removeInvalidSegments
//{
//    NSEnumerator *bodyEnum = [segments objectEnumerator];
//    NSValue *bodyPointer = [bodyEnum nextObject];
//    NSMutableArray *remove = [[NSMutableArray alloc] init];
//    while(bodyPointer != nil)
//    {
//        b2Body *body = (b2Body*)[bodyPointer pointerValue];
//        if ([self checkInvalidSegment:body]) {
//            CCSprite* sprite = (__bridge CCSprite*)body->GetUserData();
//            [self removeChild:sprite cleanup:YES];
//            world->DestroyBody(body);
//            [remove addObject:bodyPointer];
//        }
//        bodyPointer = [bodyEnum nextObject];
//    }
//    for (int i = 0; i < (int)[remove count]; ++i) {
//        [segments removeObject:[remove objectAtIndex:i]];
//    }
//}


@end
