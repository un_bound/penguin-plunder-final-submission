//
//  ScoreLayer.m
//  TurtleBanditProto
//
//  Created by MIT on 1/30/13.
//
//

#import "ScoreLayer.h"

@implementation ScoreLayer
-(id) init
{
    if ((self = [super init]))
	{
        
        [self scheduleUpdate];
    }
    return self;
}
- (void)update:(ccTime)Delta
{
    
}
+(id) scene: (int) num
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	ScoreLayer *layer = [ScoreLayer node];
	[layer setStuff:num];
	// add layer as a child to scene
	[scene addChild: layer];
	
    
	// return the scene
    return scene;
}
- (void)setStuff:(int)num
{
  
}
@end
