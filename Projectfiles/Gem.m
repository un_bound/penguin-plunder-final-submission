//
//  Gem.m
//  TurtleBanditProto
//
//  Created by OEIT on 1/25/13.
//
//

#import "Gem.h"

@implementation Gem

+(NSString*) generateRandomSpriteName
{
    NSArray *spriteNames = [NSArray arrayWithObjects:@"blue-gem.png", @"green-gem.png", @"purple-gem.png", @"red-gem.png", nil];
    float randomIndex = CCRANDOM_0_1()*(float)[spriteNames count];
    return [spriteNames objectAtIndex:(NSInteger)randomIndex];
}
+(NSString*) generateRequiredSpriteName
{
    return @"star-gem.png";
}
-(id) initWithRandomSprite
{
    if(self = [super initWithFile:[Gem generateRandomSpriteName]])
    {
        required = NO;
    }
    return self;
}
-(id) initWithRequiredSprite
{
    if(self = [super initWithFile:[Gem generateRequiredSpriteName] ])
    {
        required = YES;
    }
    return self;
}


@synthesize required;

@end
