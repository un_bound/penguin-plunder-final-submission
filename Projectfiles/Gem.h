//
//  Gem.h
//  TurtleBanditProto
//
//  Created by OEIT on 1/25/13.
//
//

#import "CCSprite.h"

@interface Gem : CCSprite

+(NSString*) generateRandomSpriteName;
+(NSString*) generateRequiredSpriteName;
-(id) initWithRandomSprite;
-(id) initWithRequiredSprite;

@property BOOL required;

@end
