/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim. 
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */

#import "cocos2d.h"
#import "Box2D.h"
#import "GLES-Render.h"

#import "ContactListener.h"
#import "CCMutableTexture2D.h"
enum
{
	kTagBatchNode,
};

@interface GameLayer : CCLayer
{
	b2World* world;
	ContactListener* contactListener;
	GLESDebugDraw* debugDraw;
    NSMutableArray *gems;
    CCSprite *penguinSprite;
    CGPoint penguinStartPos;
    CCSprite *gray;
    b2Body *penguinBody;
    CCSprite *goalSprite;
    
    CCSprite *Ice_Sprite;
    int ice_pixels[480][320];
    
    int Current_Level;
    
    int dir_save[480][320];
    int Score;
    
    int Current_State;
    int numGems_notRequired;
    
    CCAction *sparkle;
    NSMutableArray *sparkleFrames;
    
    CCSprite *menu_Button;
    CCSprite *menu;
    bool start_Flag;
}
+(id) scene:(int)level;
@end
