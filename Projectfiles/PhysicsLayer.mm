/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim.
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */

#import "PhysicsLayer.h"
#import "Box2DDebugLayer.h"
#import "SimpleAudioEngine.h"
#import "StartLayer.h"

//Pixel to metres ratio. Box2D uses metres as the unit for measurement.
//This ratio defines how many pixels correspond to 1 Box2D "metre"
//Box2D is optimized for objects of 1x1 metre therefore it makes sense
//to define the ratio so that your most common object type is 1x1 metre.
const float PTM_RATIO = 32.0f;
const float SPRITE_SCALE = 0.1f;
const int TILESIZE = 32;
const int TILESET_COLUMNS = 9;
const int TILESET_ROWS = 19;
CGPoint prevPoint;
NSMutableArray *gems;
CCSprite *turtleSprite;
b2Body *turtleBody;
CCSprite *goal;
float minSegmentLen = 10;
float timeStep = 0.0f;

@interface PhysicsLayer (PrivateMethods)
-(void) enableBox2dDebugDrawing;
-(void) addSomeJoinedBodies:(CGPoint)pos;
-(void) addCircleSpriteAt:(CGPoint)p;
-(b2Vec2) toMeters:(CGPoint)point;
-(CGPoint) toPixels:(b2Vec2)vec;
@end

@implementation PhysicsLayer

-(id) init
{
	if ((self = [super init]))
	{
		CCLOG(@"%@ init", NSStringFromClass([self class]));
		glClearColor(0.1f, 0.0f, 0.2f, 1.0f);
		
		// Construct a world object, which will hold and simulate the rigid bodies.
		b2Vec2 gravity = b2Vec2(0.0f, -10.0f);
		world = new b2World(gravity);
		world->SetAllowSleeping(YES);
		//world->SetContinuousPhysics(YES);
		[self pausePhysics];
        
		// uncomment this line to draw debug info
//		[self enableBox2dDebugDrawing];
        
		contactListener = new ContactListener();
		world->SetContactListener(contactListener);
		
		// for the screenBorder body we'll need these values
		CGSize screenSize = [CCDirector sharedDirector].winSize;
		float widthInMeters = screenSize.width / PTM_RATIO;
		float heightInMeters = screenSize.height / PTM_RATIO;
		b2Vec2 lowerLeftCorner = b2Vec2(0, 0);
		b2Vec2 lowerRightCorner = b2Vec2(widthInMeters, 0);
		b2Vec2 upperLeftCorner = b2Vec2(0, heightInMeters);
		b2Vec2 upperRightCorner = b2Vec2(widthInMeters, heightInMeters);
		
		// Define the static container body, which will provide the collisions at screen borders.
		b2BodyDef screenBorderDef;
		screenBorderDef.position.Set(0, 0);
		b2Body* screenBorderBody = world->CreateBody(&screenBorderDef);
		b2EdgeShape screenBorderShape;
		
		// Create fixtures for the four borders (the border shape is re-used)
		screenBorderShape.Set(lowerLeftCorner, lowerRightCorner);
		screenBorderBody->CreateFixture(&screenBorderShape, 0);
		screenBorderShape.Set(lowerRightCorner, upperRightCorner);
		screenBorderBody->CreateFixture(&screenBorderShape, 0);
		screenBorderShape.Set(upperRightCorner, upperLeftCorner);
		screenBorderBody->CreateFixture(&screenBorderShape, 0);
		screenBorderShape.Set(upperLeftCorner, lowerLeftCorner);
		screenBorderBody->CreateFixture(&screenBorderShape, 0);
        
        CCMenu * controlMenu = [CCMenu menuWithItems:nil];
        CCMenuItemImage *goButton = [CCMenuItemImage itemWithNormalImage:@"segment.png" selectedImage:@"blue-circle.png" target:self selector:@selector(goButtonAction)];
        goButton.position = ccp(screenSize.width/2-50, screenSize.height/2-20);
        [controlMenu addChild:goButton];
        CCMenuItemImage *resetButton = [CCMenuItemImage itemWithNormalImage:@"segment.png" selectedImage:@"blue-circle.png" target:self selector:@selector(resetButtonAction)];
        resetButton.position = ccp(screenSize.width/2-50, screenSize.height/2-70);
        [controlMenu addChild:resetButton];
        [self addChild:controlMenu];
        
        b2Body *turtleBody;
        turtleSprite = [CCSprite spriteWithFile:@"Turtle.png"];
        CGPoint turtleStartPos = ccp(50, screenSize.height - 50);
        turtleSprite.position = turtleStartPos;
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        bodyDef.position = [self toMeters:turtleStartPos];
        bodyDef.userData = (__bridge void*)turtleSprite;
        turtleBody = world->CreateBody(&bodyDef);
        b2CircleShape turtleShape;
        turtleShape.m_radius = turtleSprite.contentSize.width/2.0f/PTM_RATIO;
        
        // Define the dynamic body fixture.
        b2FixtureDef turtleFixture;
        turtleFixture.shape = &turtleShape;
        turtleFixture.density = 0.3f;
        turtleFixture.friction = 0.01f;
        turtleFixture.restitution = 0.0f;
        
        gems = [[NSMutableArray alloc] init];
        
        CCSprite *gemSprite;
        CGPoint gemPos;
        
        gemSprite = [CCSprite spriteWithFile:@"blue-gem.png"];
        gemPos = ccp(100.0f, 100.0f);
        gemSprite.position = gemPos;
        [gems addObject:gemSprite];
        [self addChild:gemSprite];
        gemSprite = [CCSprite spriteWithFile:@"purple-gem.png"];
        gemPos = ccp(200.0f, 100.0f);
        gemSprite.position = gemPos;
        [gems addObject:gemSprite];
        [self addChild:gemSprite];
//        gemSprite = [CCSprite spriteWithFile:@"star-gem.png"];
//        gemPos = ccp(300.0f, 200.0f);
//        gemSprite.position = gemPos;
//        [gems addObject:gemSprite];
//        [self addChild:gemSprite];
        
        goal = [CCSprite spriteWithFile:@"green-gem.png"];
        goal.position = ccp(screenSize.width-50, 50);
        [self addChild:goal];
        
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"explo2.wav"];
        
        turtleBody->CreateFixture(&turtleFixture);
        [self addChild:turtleSprite];

        
//        b2Vec2 vertexArray[20];
//        vertexArray[0].Set(screenSize.width, 0.0f);
//        vertexArray[1].Set(0.0f, 0.0f);
//        
//		for (int i = 2; i < 20; i++)
//		{
//            vertexArray[i].Set(screenSize.width/18.0f*i/PTM_RATIO, pow(i,2)/400.0f * screenSize.height/PTM_RATIO);
//		}
//		b2ChainShape chain;
//        chain.CreateChain(vertexArray, 20);
        
        b2BodyDef floorDef;
		floorDef.position.Set(0, 0);
//		b2Body* floorBody = world->CreateBody(&floorDef);
//		floorBody->CreateFixture(&chain, 0);
        
		[self scheduleUpdate];
		
		[KKInput sharedInput].accelerometerActive = YES;
	}
    
	return self;
}

-(void) goButtonAction
{
    [self resumePhysics];
}

-(void) resetButtonAction
{
    [[CCDirector sharedDirector] replaceScene:[PhysicsLayer scene]];
    
}

-(void) dealloc
{
	delete contactListener;
	delete world;
    
#ifndef KK_ARC_ENABLED
	[super dealloc];
#endif
}

-(void) addCircleSpriteAt:(CGPoint)pos isStatic:(BOOL)isStatic
{
	b2Body *body;
    CCSprite *sprite = [CCSprite spriteWithFile:@"blue-circle.png"];
    [sprite setScale:SPRITE_SCALE];
    sprite.anchorPoint = ccp(0.5, 0.5);
	sprite.position = pos;
	b2BodyDef bodyDef;
    if(isStatic)
    {
        bodyDef.type = b2_staticBody;
        
    }
    else
    {
        bodyDef.type = b2_dynamicBody;
        
    }
    bodyDef.position = [self toMeters:pos];
    bodyDef.userData = (__bridge void*)sprite;
    body = world->CreateBody(&bodyDef);
    b2CircleShape circle;
    float radius = ((__bridge CCSprite*)body->GetUserData()).contentSize.width/2.0f*SPRITE_SCALE/PTM_RATIO;
	circle.m_radius = radius;
	
	// Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circle;
	fixtureDef.density = 0.3f;
	fixtureDef.friction = 0.0f;
	fixtureDef.restitution = 0.0f;
    
	body->CreateFixture(&fixtureDef);
    [self addChild:sprite];
}

-(void) addSegmentAtPoint:(CGPoint)pos atRadianAngle:(float) angle
{
	b2Body *body;
    CCSprite *sprite = [CCSprite spriteWithFile:@"segment.png"];
    sprite.anchorPoint = ccp(0.5, 0.5);
	sprite.position = pos;
	b2BodyDef bodyDef;
    bodyDef.type = b2_staticBody;
    bodyDef.position = [self toMeters:pos];
    bodyDef.angle = angle;
    bodyDef.userData = (__bridge void*)sprite;
    body = world->CreateBody(&bodyDef);
    b2PolygonShape box;
    box.SetAsBox(minSegmentLen/PTM_RATIO, 2.0f/PTM_RATIO);//sprite.contentSize.height/PTM_RATIO/2.0f);
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &box;
	fixtureDef.density = 0.3f;
	fixtureDef.friction = 0.01f;
    
	body->CreateFixture(&fixtureDef);
    [self addChild:sprite];
}

-(void) enableBox2dDebugDrawing
{
	// Using John Wordsworth's Box2DDebugLayer class now
	// The advantage is that it draws the debug information over the normal cocos2d graphics,
	// so you'll still see the textures of each object.
	const BOOL useBox2DDebugLayer = YES;
    
	
	float debugDrawScaleFactor = 1.0f;
#if KK_PLATFORM_IOS
	debugDrawScaleFactor = [[CCDirector sharedDirector] contentScaleFactor];
#endif
	debugDrawScaleFactor *= PTM_RATIO;
    
	UInt32 debugDrawFlags = 0;
	debugDrawFlags += b2Draw::e_shapeBit;
	debugDrawFlags += b2Draw::e_jointBit;
	//debugDrawFlags += b2Draw::e_aabbBit;
	//debugDrawFlags += b2Draw::e_pairBit;
	//debugDrawFlags += b2Draw::e_centerOfMassBit;
    
	if (useBox2DDebugLayer)
	{
		Box2DDebugLayer* debugLayer = [Box2DDebugLayer debugLayerWithWorld:world
																  ptmRatio:PTM_RATIO
																	 flags:debugDrawFlags];
		[self addChild:debugLayer z:100];
	}
	else
	{
		debugDraw = new GLESDebugDraw(debugDrawScaleFactor);
		if (debugDraw)
		{
			debugDraw->SetFlags(debugDrawFlags);
			world->SetDebugDraw(debugDraw);
		}
	}
}

//-(void) addSomeJoinedBodies:(CGPoint)pos
//{
//	// Create a body definition and set it to be a dynamic body
//	b2BodyDef bodyDef;
//	bodyDef.type = b2_dynamicBody;
//
//	// position must be converted to meters
//	bodyDef.position = [self toMeters:pos];
//	bodyDef.position = bodyDef.position + b2Vec2(-1, -1);
//	bodyDef.userData = (__bridge void*)[self addCircleSpriteAt:pos];
//	b2Body* bodyA = world->CreateBody(&bodyDef);
//	[self bodyCreateFixture:bodyA];
//
//	bodyDef.position = [self toMeters:pos];
//	bodyDef.userData = (__bridge void*)[self addCircleSpriteAt:pos];
//	b2Body* bodyB = world->CreateBody(&bodyDef);
//	[self bodyCreateFixture:bodyB];
//
//	bodyDef.position = [self toMeters:pos];
//	bodyDef.position = bodyDef.position + b2Vec2(1, 1);
//	bodyDef.userData = (__bridge void*)[self addCircleSpriteAt:pos];
//	b2Body* bodyC = world->CreateBody(&bodyDef);
//	[self bodyCreateFixture:bodyC];
//
//	b2RevoluteJointDef jointDef;
//	jointDef.Initialize(bodyA, bodyB, bodyB->GetWorldCenter());
//	bodyA->GetWorld()->CreateJoint(&jointDef);
//
//	jointDef.Initialize(bodyB, bodyC, bodyC->GetWorldCenter());
//	bodyA->GetWorld()->CreateJoint(&jointDef);
//
//	// create an invisible static body to attach to
//	bodyDef.type = b2_staticBody;
//	bodyDef.position = [self toMeters:pos];
//	b2Body* staticBody = world->CreateBody(&bodyDef);
//	jointDef.Initialize(staticBody, bodyA, bodyA->GetWorldCenter());
//	bodyA->GetWorld()->CreateJoint(&jointDef);
//}

//-(void) addNewSpriteAt:(CGPoint)pos
//{
//	// Create a body definition and set it to be a dynamic body
//	b2BodyDef bodyDef;
//	bodyDef.type = b2_staticBody;
//
//	// position must be converted to meters
//	bodyDef.position = [self toMeters:pos];
//
//	// assign the sprite as userdata so it's easy to get to the sprite when working with the body
//    CCSprite *sprite = (__bridge void*)[self addCircleSpriteAt:pos];
//	bodyDef.userData = sprite;
//	b2Body* body = world->CreateBody(&bodyDef);
//
//	[self bodyCreateFixture:body];
//}

-(void) update:(ccTime)delta
{
    KKInput* input = [KKInput sharedInput];
    if(input.anyTouchBeganThisFrame)
    {
        prevPoint = [input locationOfAnyTouchInPhase:KKTouchPhaseBegan];
        [self pausePhysics];
    }
    else if (input.anyTouchEndedThisFrame)
    {
        [self addCircleSpriteAt:[input locationOfAnyTouchInPhase:KKTouchPhaseEnded] isStatic:NO];
    }
    else if (input.anyTouchLocation.x != 0.0f && input.anyTouchLocation.y != 0.0f)
    {
        CGPoint newPoint = [input locationOfAnyTouchInPhase:KKTouchPhaseAny];
        float deltaX = newPoint.x - prevPoint.x;
        float deltaY = newPoint.y - prevPoint.y;
        float distFromLast = sqrt(deltaX*deltaX + deltaY*deltaY);
        if(distFromLast > minSegmentLen)
        {
            float scaledDeltaX = deltaX/distFromLast*minSegmentLen;
            float scaledDeltaY = deltaY/distFromLast*minSegmentLen;
            [self addSegmentAtPoint:ccp(prevPoint.x+scaledDeltaX/2, prevPoint.y+scaledDeltaY/2) atRadianAngle: (atan2f(deltaY, deltaX))];
            prevPoint = ccp(prevPoint.x+ scaledDeltaX, prevPoint.y+ scaledDeltaY);
        }
    }
	// The number of iterations influence the accuracy of the physics simulation. With higher values the
	// body's velocity and position are more accurately tracked but at the cost of speed.
	// Usually for games only 1 position iteration is necessary to achieve good results.
	int32 velocityIterations = 8;
	int32 positionIterations = 1;
	world->Step(timeStep, velocityIterations, positionIterations);
	
    for (b2Body* body = world->GetBodyList(); body != nil; body = body->GetNext())
    {
        //get the sprite associated with the body
        CCSprite* sprite = (__bridge CCSprite*)body->GetUserData();
        if (sprite != NULL)
        {
            // update the sprite's position to where their physics bodies are
            sprite.position = [self toPixels:body->GetPosition()];
            float angle = body->GetAngle();
            sprite.rotation = CC_RADIANS_TO_DEGREES(angle) * -1;
        }
    }
    [self detectCollisions];
    if(turtleSprite.position.x < goal.position.x+ goal.contentSize.width/2.0f && turtleSprite.position.x> goal.position.x - goal.contentSize.width/2.0f)
    {
        if(turtleSprite.position.y < (goal.position.y + goal.contentSize.height/2.0f) && goal.position.y > goal.position.y - goal.contentSize.height/2.0f)
        {
            if([gems count] ==0)
            {
                [[CCDirector sharedDirector] replaceScene:[StartLayer scene]];
            }
        }
    }
}

-(void) detectCollisions
{
    for(int j = 0; j < [gems count]; j++)
    {
            NSInteger gemIndex = j;
            CCSprite *gemSprite = [gems objectAtIndex:gemIndex];
            
//            firstrect = [gemSprite textureRect];
//            secondrect = [turtleSprite textureRect];
            //check if their x coordinates match
            if(turtleSprite.position.x < gemSprite.position.x+ gemSprite.contentSize.width/2.0f && turtleSprite.position.x> gemSprite.position.x - gemSprite.contentSize.width/2.0f)
            {
                //check if their y coordinates are within the height of the block
                if(turtleSprite.position.y < (gemSprite.position.y + gemSprite.contentSize.height/2.0f) && turtleSprite.position.y > gemSprite.position.y - gemSprite.contentSize.height/2.0f)
                {
                    [self removeChild:gemSprite cleanup:YES];
                    [gems removeObjectAtIndex:gemIndex];
                    [[SimpleAudioEngine sharedEngine] playEffect:@"explo2.wav"];
                    
                }
            }
        
    }

}

-(void) pausePhysics
{
    timeStep = 0;
}

-(void) resumePhysics
{
    timeStep = .03;
}

// convenience method to convert a CGPoint to a b2Vec2
-(b2Vec2) toMeters:(CGPoint)point
{
	return b2Vec2(point.x / PTM_RATIO, point.y / PTM_RATIO);
}

// convenience method to convert a b2Vec2 to a CGPoint
-(CGPoint) toPixels:(b2Vec2)vec
{
	return ccpMult(CGPointMake(vec.x, vec.y), PTM_RATIO);
}

+(id) scene
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	PhysicsLayer *layer = [PhysicsLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
    return scene;
}

//#if DEBUG
//-(void) draw
//{
//	[super draw];
//
//	if (debugDraw)
//	{
//		ccGLEnableVertexAttribs(kCCVertexAttribFlag_Position);
//		kmGLPushMatrix();
//		world->DrawDebugData();
//		kmGLPopMatrix();
//	}
//}
//#endif

@end
